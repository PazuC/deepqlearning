class Snake {
    constructor(x, y, xspeed, yspeed, colorR, colorG, colorB) {
        this.x = x
        this.y = y
        this.xspeed = xspeed
        this.yspeed = yspeed
        this.colorR = colorR
        this.colorG = colorG
        this.colorB = colorB
        this.total = 0;
        this.tail = [];
        this.tailLocationArr = [];
        // this.total = 5;
        // this.tail = [createVector(0, 0), createVector(0, 0), createVector(0, 0), createVector(0, 0), createVector(0, 0)];
    }

    eat(pos) {
        var d = dist(this.x, this.y, pos.x, pos.y)
        if (d < 1) {
            this.total++; // eat food and length increase
            return true;
        } else {
            return false;
        }

    }
    dir(x, y) {
        this.xspeed = x;
        this.yspeed = y
    }

    death() {
        for (var i = 0; i < this.tail.length; i++) {
            var pos = this.tail[i];
            var d = dist(this.x, this.y, pos.x, pos.y);
            if (d < 1) {
                this.total = 0;
                this.tail = [];
                this.tailLocationArr = [];
            }
        }
    }

    tailLocation() {
        for (var i = 0; i < this.tail.length; i++) {
            var pos = this.tail[i];
            this.tailLocationArr[i] = pos;

        }
        return this.tailLocationArr
    }

    update() {
        if (this.total === this.tail.length) {
            for (var i = 0; i < this.tail.length - 1; i++) {
                this.tail[i] = this.tail[i + 1];
            }
        }
        this.tail[this.total - 1] = createVector(this.x, this.y)

        this.x = this.x + this.xspeed * scl;
        this.y = this.y + this.yspeed * scl;

        this.x = constrain(this.x, 0, width - scl)
        this.y = constrain(this.y, 0, height - scl)

    }

    show() {
        fill(this.colorR, this.colorG, this.colorB);

        for (var i = 0; i < this.tail.length; i++) {
            fill(this.colorR + 2 * i, this.colorG + 2 * i, this.colorB + 2 * i)
            noStroke();
            rect(this.tail[i].x, this.tail[i].y, scl, scl)
        }

        rect(this.x, this.y, scl, scl)

    }
}